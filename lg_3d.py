# calculation of light transmission in a 3d light guide

import math
import sys
import csv
from random import seed
from random import random
import numpy as np
import matplotlib.pyplot as plt

# Calculate a random angle from 0 to 360 degrees
def RandomAngle(): 
    return random() * 360

# Calculate a random x (or y) coordinate
def RandomCoordinate(side):
    x = (random() * 2 * side) - side
    return x

# Simulate absorption probabity at a reflective surface
def IsAbsorbed(reflectivity):
    if (random() < reflectivity):
        return 0
    return 1

#RMS calculation of xy efficiency
def RMS_XY(succ, datax, datay, xybins, x0):
    point = 0
    average = (1.0 * succ)/(xybins*xybins) 
    bins=np.zeros((xybins,xybins),dtype=float)
    while (point < succ):
        xbin = int((xybins*(datax[point]+x0))/(2*x0))
        ybin = int((xybins*(datay[point]+x0))/(2*x0))
        bins[xbin,ybin]=bins[xbin,ybin]+1.0
        point = point + 1
    ybin = 0
    sumsquares = 0.0
    while (ybin < xybins):
        xbin = 0
        while(xbin < xybins):
            diff = ((1.0*bins[xbin,ybin]-average)/average)
            sumsquares = sumsquares + (diff*diff)
            xbin = xbin + 1
        ybin = ybin + 1
    meansquares = sumsquares/(xybins * xybins)
    return math.sqrt(meansquares)

# Calculate the half-width and wall angle of a pyramidal light guide at height Z
# Inputs: HT (total height), X0: (half-width of base), XF (Half-width of PM at the top)

def CalcSide(Z, X0, HT, XF, Mode):
    if (Mode == 'PSimple'):
        Side, WallAngle = CalcSideSimple (Z, X0, HT, XF)

    if (Mode == 'P2Step'):
        Side, WallAngle = CalcSide2Step (Z, X0, HT, XF)

    return [Side, WallAngle]
        
def CalcSideSimple (Z, X0, HT, XF):
    WallAngle = math.atan((X0-XF)/HT)
    Side = X0 - (Z*math.tan(WallAngle)) 
    return [Side, WallAngle]

def CalcSide2Step(Z, X0, HT, XF): #2-step pyramid
    W = 1.0 # Factor to increase the width of the bottom half of the pyramid
    V = 0.3
    WallAngleLower = math.atan((X0-(XF*W))/HT)
    if (Z <= (HT*V)):
        WallAngle = WallAngleLower
        Side = X0-(Z*math.tan(WallAngle))
    else:
        newbase = X0-(HT*V)*math.tan(WallAngleLower)
        WallAngle = math.atan((newbase-XF)/(HT*(1.0-V)))
        Side = newbase - (Z-(HT*V))*math.tan(WallAngle)
    return [Side, WallAngle]

def CalcNewZ (Z, DeltaX, Theta, Phi, x0, ht, xf, vertical, Mode):
    if (Mode == 'PSimple'):  #Simple pyramid
        wall_angle = math.atan((x0-xf)/ht)
        if (vertical==1):
            NewZ = Z + DeltaX/(math.cos(Phi)*(math.tan(wall_angle) + math.tan(Theta)))
        else:
            NewZ = Z + DeltaX/(math.sin(Phi)*(math.tan(wall_angle) + math.tan(Theta)))
        NewTheta = Theta + (2.0 * wall_angle)

    if (Mode == 'P2Step'):  #2-step pyramid
        side, wall_angle2 = CalcSide2Step((ht*0.99), x0, ht, xf)
        side, wall_angle1 = CalcSide2Step(0, x0, ht, xf)

        if (vertical==1):
            Z1 = Z + DeltaX/(math.cos(Phi)*(math.tan(wall_angle1) + math.tan(Theta)))
            Z2 = Z + DeltaX/(math.cos(Phi)*(math.tan(wall_angle2) + math.tan(Theta)))
        else:
            Z1 = Z + DeltaX/(math.sin(Phi)*(math.tan(wall_angle1) + math.tan(Theta)))
            Z2 = Z + DeltaX/(math.sin(Phi)*(math.tan(wall_angle2) + math.tan(Theta)))


        if (Z1 < Z2):
            NewZ = Z1
            wall_angle = wall_angle1
        else:
            NewZ = Z2
            wall_angle = wall_angle2
            
        NewTheta = Theta + (2.0*wall_angle)
        
    return [NewZ, NewTheta]
    
#Generate a theta distribution for Cherenkov light from a non-normal particle
def CherenkovDist(IncidentAngle, NGlass, NLG):
    CherenkovAngle = math.acos(1/NGlass)
    phi=math.radians(RandomAngle())
    Theta1=CherenkovAngle+(IncidentAngle*math.cos(phi)) #Modified Cherenkov angle in the glass
    if (abs(math.sin(Theta1)*(NGlass/NLG))>1): #Internal reflection
        if (Theta1 > (math.pi/2)):
            return (Theta1)
        else:
            return ((math.pi/2)-Theta1)         
    Theta2=math.asin(math.sin(Theta1)*(NGlass/NLG)) #Snell's law for refraction from glass to the LG
    return Theta2
    
def diffuser(theta, phi):  # Reference: "https://doi.org/10.1002/j.2168-0159.2013.tb06203.x"
    # The reference is a characterization of a 200 micron thick diffusing film with 2 micron 
    # embedded particles. The parameters should be adjusted for the diffuser material used

    t = 1.0 # unit film thickness
    
    if (theta < 0.05):
        theta = 0.05  # avoid divide-by-zeros
        
    d = t/math.cos(theta) # effective path length for light going through the diffuser
    
    # sigma = math.radians(21.8 * math.pow(d, 1.36)) # Width of the gaussian peak. Need to figure out units
    sigma = math.radians(25.5)  # Fixed value for 63 degree angle of incidence
    # sigma = math.radians(0.1) # Turn off diffuser
    
    c = 0.4 # For calculating the scattering profile.
            # It is correct if the light is scattered twice, and an approximation if scattered more than twice.

    A = math.exp(c*d) # Intensity of the Gaussian scattering profile

    scatter = 0
    while(scatter < 1):
        ThetaScat = math.radians(RandomAngle()/4)
        PhiScat = phi  # The phi change in the scattering angle can be ignored for the purposes of this MC
        I = (A / (math.sqrt(2*math.pi)*sigma))*math.exp(-1*((ThetaScat-theta)**2)/(2*(sigma**2)))
        if (random()<I):
            scatter = 1
            
    #print (theta, ThetaScat, phi, PhiScat)
    return [ThetaScat, PhiScat]

# Function to calculate the next reflection at the light guide surface. 
def reflection(x, y, z, theta, phi, x0, ht, xf, debug):

    mode='PSimple'
    #mode='P2Step'

    side, wall_angle = CalcSide(z, x0, ht, xf, mode)
    
    #start by figuring out which wall will be hit next
    if (math.cos(phi) > 0): # positive x direction
        xsign = 1
        yproj = y + ((side-x)*math.tan(phi))
    else:
        xsign = -1
        yproj = y - ((x+side)*math.tan(phi))
        
    if (math.sin(phi) > 0): # positive y direction
        ysign = 1
        xproj = x + ((side-y)/math.tan(phi))
    else:
        ysign = -1
        xproj = x - ((y+side)/math.tan(phi))

    if (debug > 0):
        print ("xproj, yproj: ", xproj, yproj)

    xprojdiff = xproj - x
    yprojdiff = yproj - y

    # Now we need to check whether the light will hit a vertical or horizontal side

    vertical = 0
    if (abs(xproj) > side):
        vertical = 1

    # next step is finding z of the intersection
    if (vertical==1): #light intersects with a vertical side
        if (xsign > 0):
            z1, theta1 = CalcNewZ (z, (side-x), theta, phi, x0, ht, xf, vertical, mode)
        else:
            z1, theta1 = CalcNewZ (z, (side+x), theta, (phi+math.pi), x0, ht, xf, vertical, mode)
        # The x coordinate is the side of the square at new z.
        newside, wall_angle = CalcSide(z1, x0, ht, xf, mode)
        if (newside<0):
            newside=0
        x1 = xsign * newside
        # Scale y down by the size of the square at the new z
        y1 = yproj * (newside/side)
        phi1 = -1 * (phi - math.pi)

    else:  #light intersects with a horizontal side
        if (ysign > 0):
            z1, theta1 = CalcNewZ (z, (side-y), theta, phi, x0, ht, xf, vertical, mode)
        else:
            z1, theta1 = CalcNewZ (z, (side+y), theta, (phi+math.pi), x0, ht, xf, vertical, mode)
        newside, wall_angle = CalcSide(z1, x0, ht, xf, mode)
        if (newside<0):
            newside=0
        y1 = ysign * newside
        x1 = xproj * (newside/side)
        phi1 = -1 * phi
    
    return [x1, y1, z1, theta1, phi1]

def raytrace(x, y, phi, incident_angle, x0, ht, xf, debug):

    wall_angle = math.atan((x0-xf)/ht)
    step = 0
    z=0                     # start at bottom of the light guide
    theta = incident_angle  # current light angle

    data = [x, y, z, math.degrees(theta), math.degrees(phi)]
    #writer.writerow(data)
    #print(data)

    rt_debug = 0

    # if (rt_debug > 0):
    #    print ("======")
    #    print (data)

    reflect = 0

    while ((z < ht) and (theta<(math.pi/2))) :
         LastTheta = theta
         step = step + 1
 
         if (phi == 0.0):
                 phi = phi + math_radians(0.005) # avoid divide-by-zero
      
         ThetaPrev = theta
         x, y, z, theta, phi = reflection(x, y, z, theta, phi, x0, ht, xf, rt_debug)
         data = [x, y, z, math.degrees(theta), math.degrees(phi)]
         if (debug == 1):
            writer.writerow(data)
            print(data)

         if (z < ht):
             reflect = reflect + 1

         if (rt_debug > 0):
             print (data)

    if (z < ht):
        LastTheta = theta
    return [z, LastTheta, reflect]

################
#              #
# Main program #
#              #
################

seed()

f = open('light_output_3d.csv', 'w+')
writer = csv.writer(f)

incident_angle = math.radians(0) # incident angle of the incoming particle

NGlass = 1.67  # Refractive index of ZF2
NLG = 1.5      # Refaective index of acrylic

#initial_angle = math.radians(63) # 63 deg in acrylic (n=1.5) from 53 degree cherenkov angle in ZF2 (n=1.67)

ht = 1.0 # total height of the light guide (in cm) 
x0 = 1.9 # half-width of the light guide base (3.8 cm total)
xf = 0.6  # half-width of the SiPM (total 1.2 cm)

ReflectionEff = 0.97  # Reflection efficiency of 200nm Al on acrylic (approximate)

print ('Initializing ')

# Header for the CSV output file

header = ['Height', 'IncidentAngle', 'Total', 'Success', 'Fail', ]
writer.writerow(header)

# Counts of how many photons reach the SiPM (or don't):

totsucc = 0
totfail = 0

# Vertical (z) height of the LG:
ht =   0.75 # Starting height
maxht = 2.0 # Maximum height to simulate
htstep = 0.125 # Steps in height to simulate

maxht = maxht + 0.01 # Hack to include the last data point


# Incident angle of the particle
incident_angle = math.radians(0)
max_angle = math.radians(30)
angle_step = math.radians(5)

max_angle = max_angle + math.radians(0.5) # Hack to include the last data point

# Number of photons to simulate per LG height

maxpoints = 361000

height = []
IncAngle  = []
dataeff = []
DistributionRMS = [] 

print('Height', 'Inc_angle', 'Succ', 'Fail', 'Efficiency', 'RMS')

# Start of loop

while ht < maxht:
#while incident_angle < max_angle:
    succ = 0
    fail = 0

# Set up data arrays for plotting:

    datax = []
    datay = []
    dataTheta1 = []
    dataThetaFinalSucc = []
    dataThetaFinalFail = []
    dataPhi1 = []
    dataRef  = []
    dataRefFail  = []
    diffth = []
    diffph = []
    
# Start of the main simulation
    
    points = 0
    while points < maxpoints:
        xinit = RandomCoordinate(x0)
        yinit = RandomCoordinate(x0)
        phi = math.radians(RandomAngle())

        initial_angle = CherenkovDist(incident_angle, NGlass, NLG) #Generate Cherenkov photon entering the diffuser

        if (initial_angle < 90):
            theta1, phi1 = diffuser(initial_angle, phi) # Simulate scattering in the diffuser
            #theta1, phi1 = [initial_angle, phi] # Simulate without the diffuser
        
            diffth.append(math.degrees(theta1-initial_angle))
            diffph.append(math.degrees(phi1-phi))

            debug = 0

            x = xinit
            y = yinit
        
            num_reflections = 0 # Number of times that the light reflects from the side
            z, theta, num_reflections  = raytrace(x, y, phi1, theta1, x0, ht, xf, debug)

            # Did the photon get absorbed at a mirrored surface?
            Absorbed = 0
            nref = 0
            while (nref < num_reflections):
                nref = nref + 1
                Absorbed = Absorbed + IsAbsorbed(ReflectionEff)

            # Fill histogram contents    
            if  ((z > ht) and (theta < (math.pi/2)) and (Absorbed==0)):
                succ = succ + 1
                datax.append(xinit)
                datay.append(yinit)
                dataRef.append(num_reflections)
                dataThetaFinalSucc.append(math.degrees(theta))
                dataTheta1.append(math.degrees(theta1))
                dataPhi1.append(math.degrees(phi1))
            else:
                fail = fail + 1
                dataThetaFinalFail.append(math.degrees(theta))
                dataRefFail.append(num_reflections)
        else:
            fail = fail + 1
            dataThetaFinalFail.append(math.degrees(initial_angle))
            dataRefFail.append(1)
            
        points = points + 1

    data =  [ht, incident_angle, succ+fail, succ, fail]
    writer.writerow(data)

# Calculate RMS of XY efficiency

    xybins = 19
    uniformity = RMS_XY(succ, datax, datay, xybins, x0)
    
    print(ht, math.degrees(incident_angle), succ, fail, (1.0*succ/(succ+fail)), uniformity)

    dataeff.append(100.0*succ/(succ+fail))
    height.append(ht)
    IncAngle.append(math.degrees(incident_angle))
    DistributionRMS.append(100*uniformity)

# Generating histograms. Uncomment xxx.show() to view    
    
    xy = plt.figure(1) # 2d histogram with initial XY coordinates for photons reaching the SiPM
    plt.hist2d(datax, datay, bins=(19,19),cmap=plt.cm.jet) 
    plt.title('xy light collection efficiency: ht=%1.2f cm' % ht)
    plt.xlabel("x (cm)")
    plt.ylabel("y (cm)")
    plt.colorbar()
    #xy.show()

    th = plt.figure(2) # Histogram of post-diffuser theta for photons reaching the SiPM  
    plt.hist(dataTheta1, bins=20)
    plt.title('Incident theta (post-diffuser) for photons reaching the SiPM')
    plt.xlabel("Post-diffuser theta")
    plt.ylabel("Photons per bin")
    #th.show()

    thsucc = plt.figure(7) # Histogram of final theta for photons reaching the SiPM  
    plt.hist(dataThetaFinalSucc, bins=20)
    plt.title('Final theta for photons reaching the SiPM')
    plt.xlabel("Theta")
    plt.ylabel("Photons per bin")
    #thsucc.show()

    thfail = plt.figure(8) # Histogram of final theta for photons failing to reach the SiPM  
    plt.hist(dataThetaFinalFail, bins=20)
    plt.title('Final theta for photons failing to reach the SiPM')
    plt.xlabel("Theta")
    plt.ylabel("Photons per bin")
    #thfail.show()

    refsucc = plt.figure(9) # Histogram number of reflections for photons reaching the SiPM  
    plt.hist(dataRef, bins=20)
    plt.title('Num. reflections for photons reaching the SiPM')
    plt.xlabel("Reflections")
    plt.ylabel("Photons per bin")
    #refsucc.show()

    reffail = plt.figure(10) # Histogram number of reflections for photons failing to reach the SiPM  
    plt.hist(dataRefFail, bins=20)
    plt.title('Num. reflections for photons failing to reach the SiPM')
    plt.xlabel("Reflections")
    plt.ylabel("Photons per bin")
    #reffail.show()

    # Final vs incident theta for photons reaching the SiPM 
    thetachange = plt.figure(11)
    plt.scatter(dataTheta1, dataThetaFinalSucc)
    plt.title('final vs incident theta for photons reaching the SiPM')
    plt.xlabel("Post-diffuser theta")
    plt.ylabel("Final Theta")
    plt.ylim([0, 100])
    #thetachange.show()


    Diffth = plt.figure(5) # Change in theta for all photons passing through the diffuser 
    plt.hist(diffth, bins=20)
    plt.title('Delta theta (diffuser)')
    plt.xlabel("Change in scattered theta")
    plt.ylabel("Photons per bin")
    #Diffth.show()

# If you want to look at histograms after each ht simulation, uncomment the following lines:
    #print ("Press return to continue")
    #raw_input() # Wait for keystroke
    
    ht = ht + htstep
    #incident_angle = incident_angle + angle_step 
    
# End of loop
    
# Plot the efficiency and uniformity curves
effcurve = plt.figure(20)

plt.scatter(height, dataeff,color='k')
#plt.scatter(IncAngle, dataeff)
plt.scatter(height, DistributionRMS,color='g')

plt.title('Efficiency (black) and 100xRMS (green)')

plt.xlabel("Light guide height (cm)")
#plt.xlabel("Incident angle (degrees)")

plt.ylabel("Percent efficiency / 100xRMS")
plt.ylim([0, 50])
effcurve.show()

# Plot the uniformity curve
rmscurve = plt.figure(21)

plt.scatter(height, DistributionRMS)
#plt.scatter(IncAngle, DistributionRMS)

plt.title('RMS of xy efficiency')

plt.xlabel("Light guide height (cm)")
#plt.xlabel("Incident angle (degrees)")

plt.ylabel("RMS of distribution")
plt.ylim([0, 50])
# rmscurve.show()


print ("Press return to continue")
raw_input() # Wait for keystroke

f.close()
