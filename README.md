# Light_Guide_Sim

A toy Monte Carlo simulation of Cherenkov light propagation from a
lead-glass block to a SiPM through a mirrored acrylic light guide

Program Files:
lg_3d.py : Simulation code

Documentation:
lg_mc_overview.pdf : Short presentation explaining the algorithm

diffuser_paper.pdf: Reference paper for the diffuser part of the simulation.
